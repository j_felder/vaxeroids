extends Area2D
class_name coviroid

#Sets up a signal and gives it a name
signal play_squish
#Variables
export var worth_points:int = 50
var linear_velocity
var screen_size = Vector2()

func _process(Delta):
	#Move Coviroid at the random speed and direction it was given
	position += linear_velocity * Delta
	#Wrap to screen, if it flies off an edge it warps to the other side
	position.x = wrapf(position.x, 0, screen_size.x)
	position.y = wrapf(position.y, 0, screen_size.y)


func _ready() -> void:
	#get the screen size (for wrapping later)
	screen_size = get_viewport_rect().size
	#set a random velocity and direction
	linear_velocity = (Vector2(1, 0).rotated(deg2rad(rand_range(0, 360))) * rand_range(25, 100))

func DestroyVirus() -> void:
	#subtract active viruses from global variables
	GlobalVars.ActiveViruses -= 1
	#send the signal to "play_squish" that will be caught by a parent node (Level)
	emit_signal("play_squish")
	#destroy this object
	queue_free()

func _on_S_Coveroid_body_entered(hit_body: Node) -> void:
	#Linked in the editor - Is called by KineticBody2D (The Player)
	if hit_body is player:
		hit_body.DestroyShip()

func _on_S_Coveroid_area_entered(hit_area: Area2D) -> void:
	#Linked by the editor - Is called by Area2D (The Bullets)
	if hit_area is bullet:
		GlobalVars.PlayerScore += worth_points
		hit_area.DestroyBullet()
		DestroyVirus()
