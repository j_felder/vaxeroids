extends coviroid

#Large coviroid inherits from coviroid (Enemy.gd)
#Sets up a signal and gives it a name
signal add_coviroid

func DestroyVirus() -> void:
	#only override one function DestroyNode (Se we can split the coviroid into 2 smaller ones
	var CurPos = self.position
	#send 2 signals that should generate 2 size 2 coviroids at current position
	.emit_signal("add_coviroid", 2, CurPos)
	.emit_signal("add_coviroid", 2, CurPos)
	#call base DestroyVirus (from original coviroid - Ennemy.gd)
	.DestroyVirus()
