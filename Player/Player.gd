extends KinematicBody2D
class_name player

#Sets up a signal and gives it a name
signal destroy_ship

# Declare member variables here.
export (int) var speed = 200
export (float) var rotation_speed = 1.5
export (float) var bullet_speed = 50
export (int) var max_bullets = 3

var screen_size = Vector2()
var velocity = Vector2()
var rotation_dir = 0
var Laser = preload("res://Player/Bullets.tscn")
var exploding:bool = false

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	#We just got here, we're not exploding yet
	exploding = false
	#Screen size for wrapping
	screen_size = get_viewport_rect().size
	#Start our Idle Animation
	$AnimatedSprite.animation = "Idle"
	#Stop the auto-loop of the Laser Sound (didn't work in the editor)
	var strm = $LaserSound.stream as AudioStreamOGGVorbis
	strm.set_loop(false)

func get_input() -> void:
	if !exploding:
		#Only accept input if not exploding
		rotation_dir = 0
		velocity = Vector2()
		if Input.is_action_pressed('ui_right'):
			#turn right
			rotation_dir += 1
		if Input.is_action_pressed('ui_left'):
			#turn left
			rotation_dir -= 1
		if Input.is_action_pressed('ui_down'):
			#go backwards
			velocity = Vector2(0, speed).rotated(rotation)
		if Input.is_action_pressed('ui_up'):
			#go forwards
			velocity = Vector2(0, -speed).rotated(rotation)
		if Input.is_action_just_pressed("ui_fire"):
			#fire a laser
			FireLaser(rotation, position)

func _physics_process(delta) -> void:
	#physics are processed every frame
	#check input in a separate function
	get_input()
	#do math on our position to move the ship
	rotation += rotation_dir * rotation_speed * delta
	position.x = wrapf(position.x, 0, screen_size.x)
	position.y = wrapf(position.y, 0, screen_size.y)
	velocity = move_and_slide(velocity)

func DestroyShip() -> void:
	if !exploding:
		#prevent multiple explosions with a variable
		exploding = true
		#Set Rotation and Velocity to 0 to stop movement when exploding
		#rotation_dir = 0
		#velocity = Vector2(0, 0)
		#play the exploding sound
		$ExplodeSound.play()
		#set our animation to exploding
		$AnimatedSprite.animation = "Exploding"
		#wait until the sound is done playing
		yield($ExplodeSound, "finished")
		#send the "destroy_ship" signal to a listening node (Level)
		emit_signal("destroy_ship")
		#destroy this object
		queue_free()

func FireLaser(direction, location) -> void:
	#if we've got room for bullets
	if GlobalVars.LiveBullets <= max_bullets:
		#play the laser sound
		$LaserSound.play()
		#create a laser node
		var b = Laser.instance()
		#add the laser as a sibling
		get_parent().add_child(b)
		#place the laser at our current position and rotation
		b.rotation = direction
		b.position = location + Vector2(0, -35).rotated(direction)
		#set the laser velocity
		b.linear_velocity = Vector2(0, -bullet_speed).rotated(direction)
		#add one to our global count
		GlobalVars.LiveBullets += 1
