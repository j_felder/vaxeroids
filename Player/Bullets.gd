extends Area2D
class_name bullet

# Declare member variables here. Examples:
var linear_velocity:Vector2

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	#Move every frame in a predetermined direction and speed
	position += linear_velocity * delta
	
func _on_VisibilityNotifier2D_viewport_exited(viewport: Viewport) -> void:
	#Linked to an existing trigger in the interface
	DestroyBullet()

func DestroyBullet() -> void:
	#Change the global variable and destroy self, can be called by Coviroids too
	GlobalVars.LiveBullets -= 1
	queue_free()
