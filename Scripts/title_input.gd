extends Node2D

#Sets up a signal and gives it a name
signal swap_scene

func get_input() -> void:
	#check if enter was pressed
	if Input.is_action_just_pressed("ui_accept"):
		#send the signal "swap_scene" with the scene to load as a prameter to a listening node (Level)
		emit_signal("swap_scene", "res://World.tscn")
		#call deferred destroys this object when it's done processing (I think)
		self.call_deferred("free")

func _physics_process(delta) -> void:
	#the only thing we want to do each frame is check for the enter key...
	get_input()
