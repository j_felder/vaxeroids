extends Node2D

#Added to a separate node (Level)
#Where the ship starts
export var StartPos:Vector2 = Vector2(518, 284)
var NewPlayer = preload("res://Player/Player.tscn")
var TitleScene = preload("res://Title.tscn")
#OnReady vars are loaded AFTER _ready and don't crash
onready var score_label:RichTextLabel = self.get_node("ScoreLabel")
onready var lives_label:RichTextLabel = self.get_node("LivesLabel")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	#Load the Title Screen
	var title = TitleScene.instance()
	#$Level and get_node() crash here, self seems to work
	self.add_child(title)
	#Connect a "signal" to the Title Screen that will call SwapScene on self
	title.connect("swap_scene", self, "SwapScene")

func _process(delta: float) -> void:
	#Update the label text every frame
	score_label.text = "Score: " + str(GlobalVars.PlayerScore)
	lives_label.text = "Lives: " + str(GlobalVars.PlayerLives)

func DestroyShip() -> void:
	GlobalVars.PlayerLives -= 1
	if GlobalVars.PlayerLives > 0:
		AddShip()

func SwapScene(new_scene: String):
	#Add the level (calling node destroys itself)
	var FirstLevel = load(new_scene)
	var level = FirstLevel.instance()
	#Adds the next level on top of the labels...
	self.add_child(level)
	#Make the labels visible (and underwater...)
	score_label.visible = true
	lives_label.visible = true
	#Add the first ship
	AddShip()

func AddShip() -> void:
	#Create and add the ship
	var p = NewPlayer.instance()
	self.add_child(p)
	p.position = StartPos
	#Connect a "signal" to the ship that calls DestroyShip on self
	p.connect("destroy_ship", self, "DestroyShip")
