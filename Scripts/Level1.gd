extends Node2D
class_name level_1

export var MaxEnemies: int
var Spawn_timer:float = 0.0
var Time:float = 1.0
var S_Coveroid = preload("res://Enemies/S_Coveroid.tscn")
var M_Coveroid = preload("res://Enemies/M_Coveroid.tscn")
var L_Coveroid = preload("res://Enemies/L_Coveroid.tscn")
var CoviTionary = {1:S_Coveroid, 2:M_Coveroid, 3:L_Coveroid}

func _process(delta: float) -> void:
	Spawn_timer += delta
	if Spawn_timer > Time:
		if GlobalVars.ActiveViruses < MaxEnemies:
			var CoviroidSize = randi()%3+1
			if CoviTionary.has(CoviroidSize):
				var NewLoc = Vector2(rand_range(0,get_viewport_rect().size.x),rand_range(0,get_viewport_rect().size.y))
				AddCoviroid(CoviroidSize, NewLoc)
		Spawn_timer = 0
		Time = rand_range(1, 3)

func PlaySquish() -> void:
	$SquishPlayer.play()

func AddCoviroid(CoSize: int, CurLoc: Vector2) -> void:
	var crs = CoviTionary[CoSize].instance()
	self.add_child(crs)
	crs.position = CurLoc
	GlobalVars.ActiveViruses += 1
	crs.connect("play_squish", self, "PlaySquish")
	if CoSize > 1 :
		crs.connect("add_coviroid", self, "AddCoviroid")
